#Perfomance

##Private Fields

###performanceValue – A uniquely assigned ID for the performance
•	String

###performance A list of the performances in a season
•	ArrayList<Performace>

###date – displays the month, day, and year of the performance to occur
•	int month
•	int day
•	int year

###time – display the time the performance is to occur
•	int hour
•	int minute
•	String timeSetting – (AM or PM)

###performance – name of performance
•	String

###name_Performance_Company
•	String

###name_Of_Actors:
•	String firstName
•	String lastName

###status – displays whether canceled or ongoing
•	Boolean

##Public methods

###performance – The constructor of the performance object
Parameters:
•	String PerformanceID – unique ID for the performance

###getPerformanceID returns the String value of the performance

###getPerfomance returns the ArrayList<Performance> containing the specific performance requested.

###addPerformance Adds the specified performance to the list of performance in the array
Parameter:
•	Performance performance – performance to be added

###removePerformace removes the performance from the list if the performance is canceled.
Parameter:
•	String performanceID: unique ID of the performance to be removed.

###getPerformanceStatus – returns the status of the performance
Parameters:
•	String status: tell if the show is ongoing or cancelled.


