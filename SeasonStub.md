#Season

##Private Fields

###seasonAvailability
•	Boolean

##SeasonArray
•	ArrayList<Productions> -- list of the productions

##Public Methods

###Season – Constructor for the season

##Connect to the Production class

###getProductionName returns to the array list of productions (Returns ArrayList<Productions>)

###addProduction – Adds a production to the list of productions 
Parameters:
•	Production productions – production to be added. 

###removeProduction – removes a production if the production is cancelled
Parameter:
•	String productionID: the unique ID of the production to be removed.
